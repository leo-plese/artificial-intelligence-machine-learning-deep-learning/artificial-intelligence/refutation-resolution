from sys import argv

orderedClauses = []
orderedClausesDerivNums = []

def getUniqueList(myLst):
    uniqList = []
    for x in myLst:
        if x not in uniqList:
            uniqList.append(x)
    return uniqList

def getClausesToDelete(clauses):
    clauses = list(clauses)
    clausesToDelete = []

    n=len(clauses)

    for i in range(n):
        curClause = clauses[i]
        for j in range(n):
            if i==j:
                continue
            if curClause <= clauses[j]:
                clausesToDelete.append(clauses[j])

    return getUniqueList(clausesToDelete)

def deleteRedundant(clauses):
    clausesToDelete = getClausesToDelete(clauses)

    clauses = [c for c in clauses if c not in clausesToDelete]

    return clauses

def checkIfClauseValid(clause):
    clause = list(clause)
    n = len(clause)

    for i in range(n):
        for j in range(n):
            if i == j:
                continue
            if clause[i] == negateClause(clause[j]):
                return True

    return False


def deleteUnimportant(clauses):
    notValidClauses = []

    for clause in clauses:
        if checkIfClauseValid(clause) == False:
            notValidClauses.append(clause)

    return notValidClauses


def selectClauses(premises, support, clauses):
    clauses = deleteRedundant(clauses)
    clauses = deleteUnimportant(clauses)


    F = set([c for c in clauses if c in premises])
    sos = [c for c in clauses if c in support]


    c12Lst = []
    for clause in sos:
        checkClauses = F | set([x for x in support if x != clause])
        for premise in checkClauses:
            c12Lst.append((premise, clause))

    return c12Lst


def plResolve(c1, c2):
    resolvents = []

    for literal1 in c1:
        for literal2 in c2:
            if literal1==negateClause(literal2):
                resolvents.append((c1 - {literal1}) | (c2-{literal2}))

    return resolvents


def plRefutationResolutionNoVerbose(F, negG):

    pSet = set(frozenset(x) for x in F)
    pSet = deleteRedundant(pSet)
    pSet = set(deleteUnimportant(pSet))

    negGset = set(frozenset(x) for x in negG)
    negGset = deleteRedundant(negGset)
    negGset = set(deleteUnimportant(negGset))

    clauses = pSet | negGset
    clauses = deleteRedundant(clauses)
    clauses = set(deleteUnimportant(clauses))

    new = set()

    while True:
        tmpNew = new.copy()
        clausesAtLevel = selectClauses(pSet, tmpNew | negGset, clauses)

        for (c1, c2) in clausesAtLevel:

            resolvents = plResolve(c1, c2)
            if len(resolvents) == 0:
                continue

            NIL = False
            for r in resolvents:
                if len(r) == 0:
                    NIL = True
            if NIL == True:
                # print("NILLLLLL!!!!")
                return True

            new |= set(frozenset(x) for x in resolvents)

        new = deleteRedundant(new)
        new = set(deleteUnimportant(new))


        clauses = deleteRedundant(clauses)
        clauses = set(deleteUnimportant(clauses))

        if new <= clauses:
            # print("nothing!!!!")
            return False
        clauses |= new
        clauses = deleteRedundant(clauses)
        clauses = set(deleteUnimportant(clauses))

        pSet &= clauses
        negGset &= clauses


def plRefutationResolution(F, negG):
    global orderedClauses

    pSet = set(frozenset(x) for x in F)
    pSet = deleteRedundant(pSet)
    pSet = set(deleteUnimportant(pSet))

    negGset = set(frozenset(x) for x in negG)
    negGset = deleteRedundant(negGset)
    negGset = set(deleteUnimportant(negGset))

    clauses = pSet | negGset
    clauses = deleteRedundant(clauses)
    clauses = set(deleteUnimportant(clauses))

    new = set()


    while True:
        tmpNew = new.copy()
        clausesAtLevel = selectClauses(pSet, tmpNew | negGset, clauses)

        derivDict = dict()
        for (c1, c2) in clausesAtLevel:

            resolvents = plResolve(c1, c2)
            if len(resolvents) == 0:
                continue

            NIL = False
            for r in resolvents:
                if len(r) == 0:
                    NIL = True
            if NIL == True:
                #print("NILLLLLL!!!!")
                orderedClauses += {"NIL"}
                derivNum1, derivNum2 = orderedClauses.index(c1), orderedClauses.index(c2)
                orderedClausesDerivNums.append((derivNum1+1,derivNum2+1))
                return True

            new |= set(frozenset(x) for x in resolvents)

            derivNum1, derivNum2 = orderedClauses.index(c1), orderedClauses.index(c2)
            for x in resolvents:
                derivDict[x] = (derivNum1+1, derivNum2+1)

        new = deleteRedundant(new)
        new = set(deleteUnimportant(new))

        derivDictNew = dict()
        for k in derivDict:
            if k in new:
                derivDictNew[k] = derivDict[k]

        for k in derivDictNew:
            if k not in orderedClauses:
                orderedClauses += [k]
                orderedClausesDerivNums.append(derivDictNew[k])

        clauses = deleteRedundant(clauses)
        clauses = set(deleteUnimportant(clauses))

        if new <= clauses:
            #print("nothing!!!!")
            return False
        clauses |= new
        clauses = deleteRedundant(clauses)
        clauses = set(deleteUnimportant(clauses))

        pSet &= clauses
        negGset &= clauses



def loadClauses(clauseFileDescr):
    premises = []
    for line in clauseFileDescr:
        line = line.strip()
        if len(line) == 0:
            continue
        if line.startswith("#"):
            continue
        premises.append(set(map(str.strip,line.lower().split(" v "))))
    if len(premises) > 0:
        goalClause = premises[-1]
        premises = premises[:-1]

    goalClauseRet = line.lower()

    negGoalClauses = []
    for clause in goalClause:
        negGoalClauses.append({negateClause(clause)})


    return premises, negGoalClauses, goalClauseRet


def doResolutionNotVerbose(premises, negGoalClauses, goalClause):
    isRefutationTrue = plRefutationResolutionNoVerbose(premises, negGoalClauses)

    if isRefutationTrue:
        print(str(goalClause) + " is true")
    else:
        print(str(goalClause) + " is unknown")


def doResolutionVerbose(premises, negGoalClauses, goalClause):
    global orderedClauses

    printSeqList = []

    for i in range(0, len(premises)):
        orderedClauses += [frozenset(premises[i])]
        pStr = " v ".join(premises[i])
        printSeqList.append("{}. {}".format(i + 1, pStr))

    printSeqList.append("==========")
    st = len(premises)
    for i in range(len(negGoalClauses)):
        orderedClauses += [frozenset(negGoalClauses[i])]
        ngStr = " v ".join(negGoalClauses[i])
        printSeqList.append("{}. {}".format(i + 1 + st, ngStr))

    printSeqList.append("==========")
    isRefutationTrue = plRefutationResolution(premises, negGoalClauses)

    if not isRefutationTrue:
        print(str(goalClause) + " is unknown")
        return

    for el in printSeqList:
        print(el)

    m = len(orderedClauses)
    lenInit = len(premises) + len(negGoalClauses)

    for i in range(lenInit, m):
        if i < m - 1:
            clStr = " v ".join(orderedClauses[i])
        else:
            clStr = orderedClauses[i]
        ord12 = orderedClausesDerivNums[i - lenInit]
        print("{}. {} ({}, {})".format(i + 1, clStr, ord12[0], ord12[1]))

    print("==========")
    print(str(goalClause) + " is true")


def doRefutationResolution(clauseFileFlag, verboseFlag):
    with open(clauseFileFlag, "r", encoding="utf-8") as clauseFileDescr:
        premises, negGoalClauses, goalClause = loadClauses(clauseFileDescr)

    if not verboseFlag:
        doResolutionNotVerbose(premises, negGoalClauses, goalClause)
    else:
        doResolutionVerbose(premises, negGoalClauses, goalClause)


def negateClause(C):
    if C[0] == "~":
        return C[1:]
    else:
        return "~" + C


def loadCookClauses(clauseFileDescr):
    premises = []
    for line in clauseFileDescr:
        line = line.strip()
        if len(line) == 0:
            continue
        if line.startswith("#"):
            continue
        premises.append(set(map(str.strip, line.lower().split(" v "))))

    return premises

def loadCookUserCommands(userCommandsFileDescr):
    userCommands = []
    goalClausesQuestion = []
    for line in userCommandsFileDescr:
        line = line.strip()
        if len(line) == 0:
            continue
        if line.startswith("#"):
            continue

        commandID = line[-2:].strip()
        clauseStr = line[:-2].lower()
        clause = set(map(str.strip, clauseStr.split(" v ")))

        if commandID == "?":
            goalClausesQuestion.append(clauseStr)

        userCommands.append((commandID, clause))

    return userCommands, goalClausesQuestion


def cookTestMode(clauseFileFlag, usrCommFlag):
    with open(clauseFileFlag, "r", encoding="utf-8") as clauseFileDescr:
        premises = loadCookClauses(clauseFileDescr)
    with open(usrCommFlag, "r", encoding="utf-8") as userCommandsFileDescr:
        userCommands, goalClausesQuestion = loadCookUserCommands(userCommandsFileDescr)

    premises = set(frozenset(x) for x in premises)

    i=0
    for x in userCommands:
        commandID = x[0]
        commandClause = x[1]
        if commandID == "?":
            negGoalClauses = []
            for c in commandClause:
                negGoalClauses.append({negateClause(c)})

            doResolutionNotVerbose(premises, negGoalClauses, goalClausesQuestion[i])
            i += 1

        elif commandID == "+":
            premises.add(frozenset(commandClause))
        elif commandID == "-":
            premises.remove(frozenset(commandClause))


def cookInteractiveMode(clauseFileFlag, verboseFlag):
    with open(clauseFileFlag, "r", encoding="utf-8") as clauseFileDescr:
        premises = loadCookClauses(clauseFileDescr)

    premises = set(frozenset(x) for x in premises)

    print("Please enter your query")
    userCommand = input()
    userCommand = userCommand.strip()

    while userCommand != "exit":
        userCommand = userCommand.strip()
        if len(userCommand) == 0:
            print("Please enter your query")
            userCommand = input()
            continue
        if userCommand.startswith("#"):
            print("Please enter your query")
            userCommand = input()
            continue

        commandID = userCommand[-2:].strip()
        clauseStr = userCommand[:-2].lower()
        commandClause = set(map(str.strip, clauseStr.split(" v ")))

        if commandID == "?":
            negGoalClauses = []
            for c in commandClause:
                negGoalClauses.append({negateClause(c)})

            if not verboseFlag:
                doResolutionNotVerbose(premises, negGoalClauses, clauseStr)
            else:
                doResolutionVerbose(list(premises), negGoalClauses, clauseStr)

            global orderedClauses
            orderedClauses = []
            global orderedClausesDerivNums
            orderedClausesDerivNums = []

        elif commandID == "+":
            premises.add(frozenset(commandClause))
            if verboseFlag == True:
                print("added "+clauseStr)
        elif commandID == "-":
            if commandClause in premises:
                premises.remove(frozenset(commandClause))
                if verboseFlag == True:
                    print("removed "+clauseStr)

        print("Please enter your query")
        userCommand = input()


if __name__=="__main__":
    taskFlag = argv[1]
    clauseFileFlag = argv[2]
    numParams = len(argv)-1
    verboseFlag = False
    usrCommFlag = None
    if numParams == 3:
        if argv[3] == "verbose":
            verboseFlag = True
        else:
            usrCommFlag = argv[3]
    elif numParams == 4:
        usrCommFlag = argv[3]
        verboseFlag = True


    if taskFlag == "resolution":
        doRefutationResolution(clauseFileFlag, verboseFlag)
    elif taskFlag == "cooking_test":
        cookTestMode(clauseFileFlag, usrCommFlag)
    elif taskFlag == "cooking_interactive":
        cookInteractiveMode(clauseFileFlag, verboseFlag)
